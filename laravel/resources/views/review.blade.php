@extends('layout')

@section('title')

Отзывы

@endsection

@section('main_content')

<h1>Форма добавления отзыва</h1>
<br>

@if($errors->any())
<div class="alert alert-danger">
    <ul>
        <br>
        @foreach($errors->all() as $error)
        <li>
            {{$error}}
        </li>
        @endforeach
    </ul>
</div>
@endif

<form method="post" action="/review/check">
    @csrf
    <input type="email" name="email" id="email" placeholder="Введите e-mail" class="form-control">
    <br>
    <input type="text" name="subject" id="subject" placeholder="Введите отзыв" class="form-control">
    <br>
    <textarea name="message" id="message" class="form-control" placeholder="Введите сообщение"></textarea>
    <br>
    <button type="submit" class="btn btn-success">Отпрвить</button>
</form>

<br>
<h2>Все отзывы</h2>

@foreach($reviews as $el)
<div class="alert alert-warning">
    <h4>{{$el->subject}}</h4>
    <b>{{$el->email}}</b>
    <p>{{$el->message}}</p>
</div>
@endforeach

@endsection