<?php

use Illuminate\Support\Facades\Route;

Route::get('/', action: 'App\Http\Controllers\MainController@home');
    
Route::get('/about', action: 'App\Http\Controllers\MainController@about');

Route::get('/review', action: 'App\Http\Controllers\MainController@review')->name(name:'review');

Route::post('/review/check', action: 'App\Http\Controllers\MainController@review_check');

/*Route::get('/user/{id}/{name}', function ($id, $name) {
    return 'Id: '.$id.' Name '.$name;
});*/
